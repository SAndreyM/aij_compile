# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 11:59:47 2020

@author: Alvl_SAM
"""

import json
import argparse
from subprocess import Popen, PIPE

test = ['./aij_compile/test/history_test.json', './aij_compile/test/literature_test.json', './aij_compile/test/social_test.json', './aij_compile/test/russian_test.json']

def cleaner(str_forclean, alphabet=set('абвгдеёжзийклмнопрстуфхцчшщъыьэюя.?!,:xa1234567890-–' + '\n')):
    bufstr = ""
    for char in str_forclean:
        if not alphabet.isdisjoint(char.lower()) or char == ' ':
            bufstr += char
    strs = bufstr.split('n')
    print(bufstr)
    return(bufstr)


def load(): #функция загрузки темы из json
    with open(test[0] ,'r', encoding='utf-8') as fh:
        data_his = json.load(fh)
    fh.close()
    with open(test[1] ,'r', encoding='utf-8') as fh:
        data_lit = json.load(fh)
    fh.close()
    with open(test[2] ,'r', encoding='utf-8') as fh:
        data_obh = json.load(fh)
    fh.close()
    with open(test[3] ,'r', encoding='utf-8') as fh:
        data_rus = json.load(fh)
    fh.close()
    
    return data_his, data_lit, data_obh, data_rus



def output(data_his, data_lit, data_obh, data_rus): 
    with open(test[0], 'w', encoding='utf-8') as f:
        f.write(json.dumps(data_his, 
                           sort_keys=False, 
                           ensure_ascii=False, 
                           indent=4, 
                           separators=(',', ': ')))
    f.close()
    with open(test[1], 'w', encoding='utf-8') as f:
        f.write(json.dumps(data_lit, 
                           sort_keys=False, 
                           ensure_ascii=False, 
                           indent=4, 
                           separators=(',', ': ')))
    f.close()
    with open(test[2], 'w', encoding='utf-8') as f:
        f.write(json.dumps(data_obh, 
                           sort_keys=False, 
                           ensure_ascii=False, 
                           indent=4, 
                           separators=(',', ': ')))
    f.close()
    with open(test[3], 'w', encoding='utf-8') as f:
        f.write(json.dumps(data_rus, 
                           sort_keys=False, 
                           ensure_ascii=False, 
                           indent=4, 
                           separators=(',', ': ')))
    f.close()



def json_test():
    out, err = Popen('python ./aij_compile/generate_transformers.py --test --subject=0 --model_type=gpt2 --model_name_or_path="./aij_compile/models/history" --k=5 --p=0.95 --length=500', shell=True, stdout=PIPE).communicate()
    out, err = Popen('python ./aij_compile/generate_transformers.py --test --subject=1 --model_type=gpt2 --model_name_or_path="./aij_compile/models/lit" --k=5 --p=0.95 --length=500', shell=True, stdout=PIPE).communicate()
    out, err = Popen('python ./aij_compile/generate_transformers.py --test --subject=2 --model_type=gpt2 --model_name_or_path="./aij_compile/models/ss" --k=5 --p=0.95 --length=500', shell=True, stdout=PIPE).communicate()
    out, err = Popen('python ./aij_compile/generate_transformers.py --test --subject=3 --model_type=gpt2 --model_name_or_path="./aij_compile/models/rus" --k=5 --p=0.95 --length=500', shell=True, stdout=PIPE).communicate()
    out, err = Popen('python ./aij_compile/generate_transformers.py --test --subject=4 --model_type=gpt2 --model_name_or_path="./aij_compile/models/rus" --k=5 --p=0.95 --length=500', shell=True, stdout=PIPE).communicate()

    
    
    
    
    

def custom_test(theme, subject):
    if subject == 0:
        out, err = Popen('python ./aij_compile/generate_transformers.py --subject=0 --model_type=gpt2 --model_name_or_path="./aij_compile/models/history" --k=5 --p=0.95 --length=500', shell=True, stdout=PIPE).communicate()
        #print(str(out, 'utf-8')) # или var = str(out, 'utf-8')
    if subject == 1:
        out, err = Popen('python ./aij_compile/generate_transformers.py --subject=0 --model_type=gpt2 --model_name_or_path="./aij_compile/models/rus" --k=5 --p=0.95 --prompt="' + theme + '" --length=500', shell=True, stdout=PIPE).communicate()
        cleaner(str(out, 'utf-8'))
        #print(str(out, 'utf-8')) # или var = str(out, 'utf-8')
    if subject == 2:
        out, err = Popen('python ./aij_compile/generate_transformers.py --subject=0 --model_type=gpt2 --model_name_or_path="./aij_compile/models/lit" --k=5 --p=0.95 --prompt="' + theme + '" --length=500', shell=True, stdout=PIPE).communicate()
        cleaner(str(out, 'utf-8'))
       # print(str(out, 'utf-8')) # или var = str(out, 'utf-8')
    if subject == 3:
        out, err = Popen('python ./aij_compile/generate_transformers.py --subject=0 --model_type=gpt2 --model_name_or_path="./aij_compile/models/ss" --k=5 --p=0.95 --prompt="' + theme + '" --length=500', shell=True, stdout=PIPE).communicate()
        cleaner(str(out, 'utf-8'))
        #print(str(out, 'utf-8')) # или var = str(out, 'utf-8')

parser = argparse.ArgumentParser()
parser.add_argument("--test", action="store_true", help="Json test")
parser.add_argument("--subject", 
                        default=None, 
                        type=int, 
                        required=True,
                        help="Custom subject for test")
parser.add_argument("--test_theme", 
                        default=None, 
                        type=str, 
                        required=True,
                        help="Custom theme for test")




args = parser.parse_args()
if args.test:
    json_test()
else:
    custom_test(args.test_theme, args.subject)
    
