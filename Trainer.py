# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 16:31:33 2020

@author: Alvl_SAM
"""

from subprocess import Popen, PIPE

def main():
    models_perplexi = []
    print("Congrats train is started")
    out, err = Popen('python ./aij_compile/pretrain_transformers.py --output_dir=./aij_compile/models/ss --model_type=gpt2 --model_name_or_path=sberbank-ai/rugpt3medium_based_on_gpt2 --do_train --train_data_file=./aij_compile/Dataset/Compile_data/TrainTest/trainSS.txt --do_eval --fp16 --eval_data_file=./aij_compile/Dataset/Compile_data/TrainTest/validSS.txt --eval_all_checkpoints --per_gpu_train_batch_size 1 --gradient_accumulation_steps 1 --num_train_epochs 1 --block_size 256 --overwrite_output_dir', shell=True, stdout=PIPE).communicate()
    models_perplexi.append(["Обществознание", str(out, 'utf-8')]) # или var = str(out, 'utf-8')
    out, err = Popen('python ./aij_compile/pretrain_transformers.py --output_dir=./aij_compile/models/rus --model_type=gpt2 --model_name_or_path=sberbank-ai/rugpt3medium_based_on_gpt2 --do_train --train_data_file=./aij_compile/Dataset/Compile_data/TrainTest/trainRus.txt --do_eval --fp16 --eval_data_file=./aij_compile/Dataset/Compile_data/TrainTest/validRus.txt --eval_all_checkpoints --per_gpu_train_batch_size 1 --gradient_accumulation_steps 1 --num_train_epochs 2 --block_size 256 --overwrite_output_dir', shell=True, stdout=PIPE).communicate()
    models_perplexi.append(["Русский язык", str(out, 'utf-8')]) # или var = str(out, 'utf-8')
    out, err = Popen('python ./aij_compile/pretrain_transformers.py --output_dir=./aij_compile/models/history --model_type=gpt2 --model_name_or_path=sberbank-ai/rugpt3medium_based_on_gpt2 --do_train --train_data_file=./aij_compile/Dataset/Compile_data/TrainTest/trainHis.txt --do_eval --fp16 --eval_data_file=./aij_compile/Dataset/Compile_data/TrainTest/validHis.txt --eval_all_checkpoints --per_gpu_train_batch_size 1 --gradient_accumulation_steps 1 --num_train_epochs 2 --block_size 256 --overwrite_output_dir', shell=True, stdout=PIPE).communicate()
    models_perplexi.append(["История", str(out, 'utf-8')]) # или var = str(out, 'utf-8')
    out, err = Popen('python ./aij_compile/pretrain_transformers.py --output_dir=./aij_compile/models/lit --model_type=gpt2 --model_name_or_path=sberbank-ai/rugpt3medium_based_on_gpt2 --do_train --train_data_file=./aij_compile/Dataset/Compile_data/TrainTest/trainLit.txt --do_eval --fp16 --eval_data_file=./aij_compile/Dataset/Compile_data/TrainTest/validLit.txt --eval_all_checkpoints --per_gpu_train_batch_size 1 --gradient_accumulation_steps 1 --num_train_epochs 2 --block_size 256 --overwrite_output_dir', shell=True, stdout=PIPE).communicate()
    models_perplexi.append(["Литература", str(out, 'utf-8')]) # или var = str(out, 'utf-8')
    for data in models_perplexi:
        print(data[0] + ":", data[1])

if __name__ == "__main__":
    main()